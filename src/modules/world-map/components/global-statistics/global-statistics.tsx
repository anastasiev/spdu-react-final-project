import {Global} from '../../typedef';
import './global-statistics.scss';

type Props = {
    global?: Global,
};

export const GlobalStatistics = ({global}: Props) => {
    if (!global) {
        return null;
    }

    return (
        <ul className='stats'>
            <li className='stats__item'><b>New Confirmed:</b> {global.NewConfirmed}</li>
            <li className='stats__item'><b>Total Confirmed:</b> {global.TotalConfirmed}</li>
            <li className='stats__item'><b>New Deaths:</b> {global.NewDeaths}</li>
            <li className='stats__item'><b>Total Deaths:</b> {global.TotalDeaths}</li>
            <li className='stats__item'><b>New Recovered:</b> {global.NewRecovered}</li>
            <li className='stats__item'><b>Total Recovered:</b> {global.TotalRecovered}</li>
        </ul>
    );
};

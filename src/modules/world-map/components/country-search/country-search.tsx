import {useState} from 'react';
import {useAppSelector} from '../../../../store/hooks';
import {worldMapSelectors} from '../../services/selectors';
import './country-search.scss';

import type {ChangeEvent} from 'react';

export const CountrySearch = () => {
    const [countryName, setCountryName] = useState('');
    const countries = useAppSelector(state => worldMapSelectors.selectAll(state));

    const filteredCountries = countries.filter(country => {
        return country.Country.toLowerCase().startsWith(countryName.toLowerCase());
    }).slice(0, 5);

    const onInputChange = (event: ChangeEvent<HTMLInputElement>) => {
        setCountryName(event.currentTarget.value);
    };

    return (
        <>
            <input
                type="text"
                onChange={onInputChange}
                value={countryName}
                className='input'
                placeholder='Search By Country'/>

            <table className='table'>
                <tbody>
                <tr>
                    <th className='table__header'>Country</th>
                    <th className='table__header'>Confirmed</th>
                    <th className='table__header'>Death</th>
                    <th className='table__header'>Recovered</th>
                </tr>

                {filteredCountries.map(country => {
                    return (
                        <tr key={country.CountryCode}>
                            <td className='table__data'>{country.Country}</td>
                            <td className='table__data'>{country.TotalConfirmed}</td>
                            <td className='table__data'>{country.TotalDeaths}</td>
                            <td className='table__data'>{country.TotalRecovered}</td>
                        </tr>
                    );
                })}
                </tbody>
            </table>
        </>
    );
};

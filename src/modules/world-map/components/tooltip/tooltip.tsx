import React from 'react';
import ReactTooltip from 'react-tooltip';
import {useAppSelector} from '../../../../store/hooks';
import {worldMapSelectors} from '../../services/selectors';
import './tooltip.scss';

export const Tooltip = () => {
    const hoveredCountryCode = useAppSelector(state => worldMapSelectors.selectHoveredCountryCode(state));
    const hoveredCountry = useAppSelector(state => worldMapSelectors.selectById(state, hoveredCountryCode));

    if (!hoveredCountry) {
        return null;
    }

    return (
        <ReactTooltip>
            <ul className='tooltip'>
                <li>{hoveredCountry.Country}</li>
                <li>New Confirmed: {hoveredCountry.NewConfirmed}</li>
                <li>Total Confirmed:{hoveredCountry.TotalConfirmed}</li>
                <li>New Deaths: {hoveredCountry.NewDeaths}</li>
                <li>Total Deaths: {hoveredCountry.TotalDeaths}</li>
            </ul>
        </ReactTooltip>
    );
};

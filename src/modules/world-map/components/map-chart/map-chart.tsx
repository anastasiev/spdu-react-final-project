import React, {memo} from 'react';
import {NavLink} from 'react-router-dom';
import {ComposableMap, Geographies, Geography, ZoomableGroup} from 'react-simple-maps';
import {useAppDispatch, useAppSelector} from '../../../../store/hooks';
import {setHoveredCountryCode, removeHoveredCountryCode} from '../../services/world-map-slice';
import {worldMapSelectors} from '../../services/selectors';
import './map-chart.scss';

export const MapChart = memo(() => {
    const geoUrl =
        'https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json';
    const dispatch = useAppDispatch();

    const countries = useAppSelector(state => worldMapSelectors.selectAll(state));

    if (!countries) {
        return null;
    }

    const maxCases = countries.reduce((memo, cur) => cur.TotalConfirmed > memo ? cur.TotalConfirmed : memo, 0);

    const getOpacity = (cases?: number) => cases ? 8 / (maxCases || 1) * cases : 1;

    const getColor = (cases?: number) => cases ? '#ff0000' : '#f5f4f6';

    return (
        <div className="map-chart-wrapper">
            <ComposableMap data-tip="">
                <ZoomableGroup>
                    <Geographies geography={geoUrl}>
                        {({geographies}) =>
                            geographies.map(geo => (
                                <NavLink to={`/country/${geo.properties.ISO_A2}`} key={geo.properties.NAME}>
                                    <Geography
                                        key={geo.rsmKey}
                                        geography={geo}
                                        onMouseEnter={() => {
                                            dispatch(setHoveredCountryCode(geo.properties.ISO_A2));
                                        }}
                                        onMouseLeave={() => {
                                            dispatch(removeHoveredCountryCode());
                                        }}
                                        style={{
                                            default: {
                                                fill: getColor(
                                                    countries.find(country => country.CountryCode === geo.properties.ISO_A2)?.TotalConfirmed
                                                ),
                                                fillOpacity: getOpacity(
                                                    countries.find(country => country.CountryCode === geo.properties.ISO_A2)?.TotalConfirmed
                                                ),
                                                outline: 'none'
                                            },
                                            hover: {
                                                fill: '#F53',
                                                outline: 'none'
                                            },
                                            pressed: {
                                                fill: '#E42',
                                                outline: 'none'
                                            }
                                        }}/>
                                </NavLink>
                            ))}
                    </Geographies>
                </ZoomableGroup>
            </ComposableMap>
        </div>
    );
});

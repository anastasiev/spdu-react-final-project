import React, {useEffect} from 'react';
import {useAppDispatch, useAppSelector} from '../../store/hooks';
import {loadWorldMapData} from './services/actions-creators';
import {worldMapSelectors} from './services/selectors';
import {MapChart} from "./components/map-chart";
import {Tooltip} from "./components/tooltip";
import {GlobalStatistics} from './components/global-statistics';
import {CountrySearch} from './components/country-search';
import './world-map.scss';

export const WorldMap = () => {
    const dispatch = useAppDispatch();
    const global = useAppSelector(state => worldMapSelectors.selectGlobal(state));

    useEffect(() => {
        dispatch(loadWorldMapData());
    }, []);

    return (
        <div className='world-map'>
            <div className='world-map__stats'>
                <GlobalStatistics global={global}/>
                <CountrySearch/>
            </div>
            <div className='world-map__chart'>
                <MapChart/>
                <Tooltip/>
            </div>
        </div>
    );
};

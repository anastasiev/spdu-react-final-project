export type Global = {
    NewConfirmed: number,
    TotalConfirmed: number,
    NewDeaths: number,
    TotalDeaths: number,
    NewRecovered: number,
    TotalRecovered: number
};

export type Country = {
    Country: string,
    CountryCode: string,
    Slug: string,
    NewConfirmed: number,
    TotalConfirmed: number,
    NewDeaths: number,
    TotalDeaths: number,
    NewRecovered: number,
    TotalRecovered: number,
    Date: Date
};

export type WorldMapData = {
    Global: Global,
    Countries: Country[]
};

export type InitialState = {
    global?: Global,
    hoveredCountryCode: string
};

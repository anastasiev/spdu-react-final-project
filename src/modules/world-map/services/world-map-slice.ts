import {createSlice, createEntityAdapter} from '@reduxjs/toolkit';
import {loadWorldMapData} from './actions-creators';
import {Country, InitialState} from '../typedef';

export const worldMapAdapter = createEntityAdapter<Country>({
    selectId: country => country.CountryCode,
    sortComparer: (countryA, countryB) => countryB.TotalConfirmed - countryA.TotalConfirmed
});

export const worldMapSlice = createSlice({
    name: 'worldMapSlice',
    initialState: worldMapAdapter.getInitialState<InitialState>({
        hoveredCountryCode: ''
    }),
    reducers: {
        setHoveredCountryCode: (state, action) => {
            state.hoveredCountryCode = action.payload;
        },
        removeHoveredCountryCode: (state) => {
            state.hoveredCountryCode = '';
        }
    },
    extraReducers: builder => builder
        .addCase(loadWorldMapData.fulfilled, (state, action) => {
            worldMapAdapter.setAll(state, action.payload.Countries);
            state.global = action.payload.Global;
        })
});

export const {setHoveredCountryCode, removeHoveredCountryCode} = worldMapSlice.actions;
export const worldMapSliceReducer = worldMapSlice.reducer;

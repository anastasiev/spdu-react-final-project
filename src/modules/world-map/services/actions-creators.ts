import {createAsyncThunk} from '@reduxjs/toolkit';
import {WorldMapData} from '../typedef';

export const loadWorldMapData = createAsyncThunk<WorldMapData>('worldMap/load', async () => {
    return fetch('https://api.covid19api.com/summary').then(result => result.json());
});

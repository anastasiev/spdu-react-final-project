import {Selector} from '@reduxjs/toolkit';
import {AppState} from '../../../store/store';
import {Global} from '../typedef';
import {worldMapAdapter} from './world-map-slice'

export const selectGlobal: Selector<AppState, Global | undefined> = state => state.worldMap.global;
export const selectHoveredCountryCode: Selector<AppState, string> = state => state.worldMap.hoveredCountryCode;

export const worldMapSelectors = {
    selectGlobal,
    selectHoveredCountryCode,
    ...worldMapAdapter.getSelectors((state: AppState) => state.worldMap)
};

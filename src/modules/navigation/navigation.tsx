import React from 'react';
import {NavLink, Outlet} from "react-router-dom";
import './navigation.scss';

export const Navigation = () => {
    return (
        <>
            <ul className='nav-list'>
                <li className='nav-list__item'>
                    <NavLink to='/map' className='nav-list__link'>Map</NavLink>
                </li>
            </ul>
            <Outlet/>
        </>
    );
};

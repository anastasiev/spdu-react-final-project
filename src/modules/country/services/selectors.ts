import {Selector} from '@reduxjs/toolkit';
import {AppState} from '../../../store/store';
import {CountryCases} from '../typedef';

const selectCountryCases: Selector<AppState, CountryCases[]> = state => state.country.all;

export const countrySelectors = {selectCountryCases};

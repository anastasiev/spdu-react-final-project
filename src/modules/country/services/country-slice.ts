import {createSlice} from '@reduxjs/toolkit';
import {loadCountryCasesData} from './actions-creators';
import {CountryCases} from '../typedef';

type InitialState = {
    all: CountryCases[]
};

const initialState: InitialState = {
    all: []
};

export const countrySlice = createSlice({
    name: 'countrySlice',
    initialState,
    reducers: {
        clear: () => initialState
    },
    extraReducers: builder => builder
        .addCase(loadCountryCasesData.fulfilled, (state, action) => {
            state.all = action.payload;
        })
});

export const {clear} = countrySlice.actions;
export const countrySliceReducer = countrySlice.reducer;

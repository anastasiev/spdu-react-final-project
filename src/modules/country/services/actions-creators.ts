import {createAsyncThunk} from '@reduxjs/toolkit';
import {CountryCases} from '../typedef';

type Arguments = {
    countryCode: string,
    from: string,
    to: string
};

export const loadCountryCasesData =
    createAsyncThunk<CountryCases[], Arguments>('country/load', async args => {
        const {countryCode, from, to} = args;
        const url = `https://api.covid19api.com/country/${countryCode}?from=${from}&to=${to}`;

        return fetch(url).then(result => result.json());
    });

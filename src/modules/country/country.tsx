import {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import {Chart} from 'react-google-charts';
import Calendar, {OnChangeDateRangeCallback} from 'react-calendar';
import {useAppDispatch, useAppSelector} from '../../store/hooks';
import {loadCountryCasesData} from './services/actions-creators';
import {clear} from './services/country-slice';
import {countrySelectors} from './services/selectors';
import 'react-calendar/dist/Calendar.css';
import './country.scss';

export const Country = () => {
    const {countryCode} = useParams();
    const dispatch = useAppDispatch();
    const countryCases = useAppSelector(state => countrySelectors.selectCountryCases(state));
    const maxDate = new Date();
    const maxPreviousDate = new Date();

    maxDate.setDate(maxDate.getDate() - 1);
    maxPreviousDate.setDate(maxPreviousDate.getDate() - 2);

    const countryCasesArray = Array.isArray(countryCases) ? countryCases : [];

    const [date, setDate] = useState({
        from: maxPreviousDate.toISOString(),
        to: maxDate.toISOString()
    });

    useEffect(() => {
        const previousDate = new Date(date.from);
        previousDate.setDate(previousDate.getDate() - 1);

        dispatch(loadCountryCasesData({
            from: previousDate.toISOString(),
            to: date.to,
            countryCode: countryCode || ''
        }));

        return () => {
            dispatch(clear());
        };
    }, [date]);

    const onCalendarChange: OnChangeDateRangeCallback = (date) => {
        setDate({
            from: new Date(date[0]).toISOString(),
            to: new Date(date[1] || date[0]).toISOString()
        });
    };

    const [firstCase, ...otherCases] = countryCasesArray;

    const data = [
        [
            {
                type: "date",
                label: "Day"
            },
            'Deaths',
            'New Cases',
            'Recovered'
        ],
        ...otherCases.map((countryCase, index) => {
            if (index === 0) {
                return [
                    new Date(countryCase.Date),
                    countryCase.Deaths - firstCase.Deaths,
                    countryCase.Confirmed - firstCase.Confirmed,
                    countryCase.Recovered - firstCase.Recovered
                ]
            }

            return [
                new Date(countryCase.Date),
                countryCase.Deaths - countryCasesArray[index - 1].Deaths,
                countryCase.Confirmed - countryCasesArray[index - 1].Confirmed,
                countryCase.Recovered - countryCasesArray[index - 1].Recovered
            ];
        })
    ];

    return (
        <div className='country'>
            <div className='country__calendar'>
                <Calendar
                    selectRange={true}
                    returnValue="range"
                    calendarType="ISO 8601"
                    maxDate={maxDate}
                    onChange={onCalendarChange}
                />
            </div>
            {!Array.isArray(countryCases) && <div>{JSON.stringify(countryCases)}</div>}
            {!!countryCases.length &&
                <div className='country__chart'>
                    <Chart
                        chartType="Line"
                        width="100%"
                        height="400px"
                        data={data}
                        options={{chart: {title: countryCases[0].Country}}}
                    />
                </div>}
        </div>
    );
};

import {combineReducers} from '@reduxjs/toolkit';
import {worldMapSliceReducer} from '../modules/world-map/services/world-map-slice';
import {countrySliceReducer} from '../modules/country/services/country-slice';

export const rootReducer = combineReducers({
    worldMap: worldMapSliceReducer,
    country: countrySliceReducer,
});

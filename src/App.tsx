import React from 'react';
import {Provider} from 'react-redux';
import {BrowserRouter, Routes, Route, Navigate} from 'react-router-dom';
import {store} from './store/store';
import {Navigation} from './modules/navigation';
import {WorldMap} from './modules/world-map';
import {Country} from './modules/country';

export const App = () => {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <Routes>
                    <Route path='/' element={<Navigation/>}>
                        <Route index element={<Navigate to='map'/>}/>
                        <Route path='map' element={<WorldMap/>}/>
                        <Route path='country/:countryCode' element={<Country/>}/>
                    </Route>
                </Routes>
            </BrowserRouter>
        </Provider>
    );
};
